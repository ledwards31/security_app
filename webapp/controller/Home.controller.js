sap.ui.define([
  "com/ventia/security_app/controller/BaseController",
  "sap/ui/model/json/JSONModel",
  "com/ventia/security_app/common/fragment.definition",
  "sap/m/MessageToast",
  "sap/m/MessageBox",
  "com/ventia/security_app/common/Import",
  "com/ventia/security_app/common/Progress.dialog",
  "com/ventia/security_app/common/AppValueHelp"
], function(Controller, JSONModel, FragmentDefinition, MessageToast, MessageBox, Import, Progress, AppValueHelp) {
  "use strict";

  return Controller.extend("com/ven.iris.secapp.controller.Home", {
    table: {},
    state: {
      audit: false
    },
    bDescending: new Map([
      ["Descending", true],
      ["Ascending", false]
    ]),
    FilterOperator: new Map([
      ["Between", sap.ui.model.FilterOperator.BT],
      ["Contains", sap.ui.model.FilterOperator.Contains],
      ["Ends With", sap.ui.model.FilterOperator.EndsWith],
      ["Equals", sap.ui.model.FilterOperator.EQ],
      ["Greater Than or Equals", sap.ui.model.FilterOperator.GE],
      ["Greater Than", sap.ui.model.FilterOperator.GT],
      ["Less Than or Equals", sap.ui.model.FilterOperator.LE],
      ["Less Than", sap.ui.model.FilterOperator.LT],
      ["Not Between", sap.ui.model.FilterOperator.NB],
      ["Not Equals", sap.ui.model.FilterOperator.NE],
      ["Not Contains", sap.ui.model.FilterOperator.NotContains],
      ["Not Ends With", sap.ui.model.FilterOperator.NotEndsWith],
      ["Not Starts With", sap.ui.model.FilterOperator.NotStartsWith],
      ["Starts With", sap.ui.model.FilterOperator.StartsWith],
    ]),
    EDM: new Map([
      ["Edm.String", () => {
        return {
          type: String,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.String()
            }
          }
        }
      }],
      ["Edm.Guid", () => {
        return {
          type: String,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Guid()
            }
          }
        }
      }],
      ["Edm.Date", () => {
        return {
          type: Date,
          control: sap.m.DatePicker,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Date(),
              constraints: {
                displayFormat: 'Date'
              }
            },
          }
        }
      }],
      ["Edm.DateTime", () => {
        return {
          type: Date,
          control: sap.m.DatePicker,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.DateTime(),
              constraints: {
                displayFormat: 'DateTime'
              }
            },
          }
        }
      }],
      ["Edm.DateTimeOffset", () => {
        return {
          type: Date,
          control: sap.m.DatePicker,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.DateTimeOffset(),
              constraints: {
                displayFormat: 'DateTime'
              }
            },
          }
        }
      }],
      ["Edm.Time", () => {
        return {
          type: Date,
          control: sap.m.TimePicker,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Time(),
            }
          }
        }
      }],
      ["Edm.TimeOfDay", () => {
        return {
          type: Date,
          control: sap.m.TimePicker,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.TimeOfDay(),
            }
          }
        }
      }],
      ["Edm.Boolean", () => {
        return {
          type: Boolean,
          control: sap.m.Switch,
          prop: "state",
          template: {
            state: {
              path: null,
              type: new sap.ui.model.odata.type.Boolean(),
            }
          }
        }
      }],
      ["Edm.Binary", () => {
        return {
          type: String,
          control: sap.m.Input,
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Raw(),
            }
          }
        }
      }],
      ["Edm.Stream", () => {
        return {
          type: String,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Stream(),
            }
          }
        }
      }],
      ["Edm.Btye", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Byte(),
            },
            type: "Number",
            maxLength: 3
          }
        }
      }],
      ["Edm.SByte", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.SByte(),
            },
            type: "Number",
            maxLength: 3
          }
        }
      }],
      ["Edm.Int16", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Int16(),
            },
            type: "Number"
          }
      }
      }],
      ["Edm.Int32", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Int32(),
            },
            type: "Number"
          }
        }
      }],
      ["Edm.Double", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Double(),
            },
            type: "Number"
          }
        }
      }],
      ["Edm.Int64", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Int64(),
            },
            type: "Number"
          }
        }
      }],
      ["Edm.Decimal", () => {
        return {
          type: Number,
          control: sap.m.Input,
          prop: "value",
          template: {
            value: {
              path: null,
              type: new sap.ui.model.odata.type.Decimal(),
            },
            type: "Number"
          }
        }
      }],
    ]),
    _onInit: async function() {
      this.model = "default";
      if (window.origin.includes("iris")) await this.defaultToIris();
      let oModel = this.getModel(this.model);
      console.log(oModel);
      let meta = new JSONModel({});
      this.ls = window.localStorage;
      this.setModel(meta, "meta");
      this.getConfig();
      if (oModel) {
        let template = this.i18n("ConnectedTo")
        this.tagChange(`${template} ${oModel.sServiceUrl}`, "Success");
      }
      this.popMeta();
      this.popConst();
      this.popEditor();
      let table = this.byId("oTbl");
      this.bus = sap.ui.getCore().getEventBus();
			this.bus.subscribe("flexible", "popOverlay", this.popOverlay, this);
      table.setBusyIndicatorDelay(0);
      this.appValueHelp = new AppValueHelp(oModel.sServiceUrl);
    },
    defaultToIris: function() {
      return new Promise((resolve, reject) => {
        let sServiceUrl = "/odata/";
        let sAuditUrl = "/audit/"
        let oModel = new sap.ui.model.odata.v4.ODataModel({
          serviceUrl: sServiceUrl,
          odataVersion: "4.0",
          operationMode: "Server",
          synchronizationMode: "None",
          groupProperties: {
            slowGroup: {
              submit: "API"
            },
            slowItem: {
              submit: "API"
            },
            fastGroup: {
              submit: "Direct"
            }
          }
        });
        let oAudit = new sap.ui.model.odata.v4.ODataModel({
          serviceUrl: sServiceUrl,
          odataVersion: "4.0",
          operationMode: "Server",
          synchronizationMode: "None",
        });
        if (oModel) {
          this.popConnection(sServiceUrl).then(() => {
            this.setModel(oModel, "default");
            let template = this.i18n("ConnectedTo")
            this.tagChange(`${template} ${oModel.sServiceUrl}`, "Success");
            this.popMeta();
            this.popConst();
            this.popEditor();
            this.appValueHelp = new AppValueHelp(oModel.sServiceUrl);
            if (oAudit) return this.popConnection(sAuditUrl);
          }).then(() => {
            this.setModel(oAudit, "audit");
          }).catch(error => {
            this.fatal(`${this.i18n("sConnectionError")}: "${error}"`);
          }).finally(() => {
            resolve();
          })
        } else {
          this.fatal(this.i18n("sConnectionError"));
        }
      })
    },
    popConnectToAService: function() {
      if (this._CTAS1) this._CTAS1.destroy();
      let oInput = new sap.m.Input();
      let oLabel = new sap.m.Label({
        text: this.i18n("ServiceURL")
      });
      let template = {
        content: [oLabel, oInput]
      }
      let form = new sap.ui.layout.form.SimpleForm(template);
      let oButtons = [
        new sap.m.Button({
          text: this.i18n("Connect"),
          type: "Emphasized",
          press: (e) => this.opConnectToAService(oInput.getValue(), "_CTAS1")
        }),
        new sap.m.Button({
          text: this.i18n("Cancel"),
          press: (e) => this.fnDestroy("_CTAS1")
        })
      ];
      this._CTAS1 = new sap.m.Dialog({
        title: this.i18n("ConnectToAService"),
        content: [form],
        buttons: oButtons
      });
      this.getView().addDependent(this._CTAS1);
      this._CTAS1.open();
    },
    opConnectToAService: function(sServiceUrl, sDialogName) {
      let oModel;
      try {
        oModel = new sap.ui.model.odata.v4.ODataModel({
          serviceUrl: sServiceUrl,
          odataVersion: "4.0",
          operationMode: "Server",
          synchronizationMode: "None",
          groupProperties: {
            slowGroup: {
              submit: "API"
            },
            slowItem: {
              submit: "API"
            },
            fastGroup: {
              submit: "Direct"
            }
          }
        });
      } catch (e) {
        this.fatal(e);
        return;
      }
      if (oModel) {
        this.popConnection(sServiceUrl).then(() => {
          this.setModel(oModel, "default");
          let template = this.i18n("ConnectedTo")
          this.tagChange(`${template} ${oModel.sServiceUrl}`, "Success");
          this.popMeta();
          this.popConst();
          this.popEditor();
          this.appValueHelp = new AppValueHelp(oModel.sServiceUrl);
          if (sDialogName) this.fnDestroy(sDialogName);
        }).catch(error => {
          this.fatal(`${this.i18n("sConnectionError")}: "${error}"`);
        })
      } else {
        this.fatal(this.i18n("sConnectionError"));
      }
    },
    popConnection: function(sServiceUrl) {
      return new Promise((resolve, reject) => {
        let sOrigin = window.location.origin;
        fetch(`${sOrigin}${sServiceUrl}$metadata`).then(response => {
          console.log(response);
          if (!response.ok) throw new Error(response.statusText);
          return response;
        }).then(() => {
          resolve()
        }).catch(error => {
          reject(error);
        })
      })
    },
    fnAudit: function (oEvent) {
      let oSource = oEvent.getSource();
      if (oSource.getPressed()) {
        this.state.audit = true;
				this.popAudit();
			} else {
        this.state.audit = false;
				this.shiftAudit();
			}
    },
    bindAudit: function() {
      console.log(this.state.audit);
      if (this.state.audit) {
        let table = this.byId("tblAudit");
        var oListBinding = table.getBinding("rows");
        let oFilter = new sap.ui.model.Filter('ObjectName', sap.ui.model.FilterOperator.EQ, this.table.name, false);
        oListBinding.filter(oFilter);
      }
    },
    popAudit: function () {
      this.shiftAll();
      let flc = this.byId("fcl");
      if (this.oScrollContainer) this.oScrollContainer.destroy();
      let names = ['ObjectType', 'ObjectName', 'ObjectID', 'TableKey', 'Action', 'User', 'Timestamp', 'Source', 'Scope', 'OldValue', 'NewValue'];
      let columns = names.map(name => {
        return new sap.ui.table.Column({
          label: new sap.m.Label({
            text: name
          }),
          template: new sap.m.Text({
            text: `{audit>${name}}`,
            maxLines: 1
          })
        });
      });
      let table = new sap.ui.table.Table({
        id: this.createId("tblAudit"),
        selectionMode: "None",
        minAutoRowCount: 10,
        threshold: 100,
        visibleRowCountMode: "Auto",
        enableCellFilter: true,
        alternateRowColors: true,
        showColumnVisibilityMenu: true,
        extension: new sap.m.OverflowToolbar({
          content: [
            new sap.m.Title({
              text: "{i18n>Auditing} {table>/name}"
            })
          ]
        }),
        rows: {
          path: `audit>/Audit`,
          filters: [new sap.ui.model.Filter('ObjectName', sap.ui.model.FilterOperator.EQ, this.table.name)],
          sorters: [new sap.ui.model.Sorter('Timestamp', true)],
          templateShareable: true,
        }
      });
      columns.forEach(column => table.addColumn(column));
      this.oScrollContainer = new sap.m.ScrollContainer({
        vertical: true,
        height: "100%",
        content: [table]
      });
			flc.addMidColumnPage(this.oScrollContainer);
			flc.setLayout(sap.f.LayoutType.TwoColumnsBeginExpanded);
    },
    popOverlay: async function (channelId, eventId, data) {
      this.shiftAll();
      let flc = this.byId("fcl");
      let row = data.parameters.row;
      let oBindingContext = row.getBindingContext(this.model);
      let path = oBindingContext.getPath();
			if (this.oScrollContainer) this.oScrollContainer.destroy();
      let template = {
        content: [],
        toolbar: new sap.m.OverflowToolbar({
          content: [
            new sap.m.Title({
              text: this.i18n("UpdatingRow")
            }),
            new sap.m.ToolbarSpacer(),
            new sap.m.Button({
              icon: "sap-icon://save",
              press: () => this.postOverlay()
            }),
            new sap.m.Button({
              icon: "sap-icon://cancel",
              press: () => this.shiftOverlay()
            })
          ]
        })
      };
      for (let e of this.table.columns) {
        let field = await this.popField(e.setting.type, this.model, e.name);
        template.content.push(new sap.m.Label({
          text: e.name
        }));
        template.content.push(field)
      };
      let oSimpleForm = new sap.ui.layout.form.SimpleForm(template);
      oSimpleForm.bindElement({
        path: `${this.model}>${path}`,
        mode: "TwoWay",
        parameters: {
          $$updateGroupId: "slowItem",
        }
      });
      this.oScrollContainer = new sap.m.ScrollContainer({
        vertical: true,
        height: "100%",
        content: [oSimpleForm]
      });
			flc.addMidColumnPage(this.oScrollContainer);
			flc.setLayout(sap.f.LayoutType.TwoColumnsBeginExpanded);
    },
    postOverlay: function() {
      this.fnSubmitBatch("slowItem").then(() => {
        let flc = this.byId("fcl");
        if (this.pOverlay) this.pOverlay.destroy();
        flc.setLayout(sap.f.LayoutType.OneColumn);
      }).catch(error => {
        MessageToast.show(error);
      })
    },
    shiftOverlay: function() {
      let flc = this.byId("fcl");
      if (this.pOverlay) this.pOverlay.destroy();
      flc.setLayout(sap.f.LayoutType.OneColumn);
      this.fnResetChanges("slowItem");
    },
    shiftAudit: function() {
      let flc = this.byId("fcl");
      if (this.pAudit) this.pAudit.destroy();
      flc.setLayout(sap.f.LayoutType.OneColumn);
    },
    shiftAll: function() {
      let flc = this.byId("fcl");
      let all = [this.pAudit, this.pOverlay];
      all.forEach(element => {
        if (element) element.destroy();
      });
      let oButton = this.byId("oBtnAudit");
      if (oButton) oButton.setPressed("false");
      flc.setLayout(sap.f.LayoutType.OneColumn);
    },
    getConfig: function() {
      let config = this.ls.getItem("config");
      if (config) {
        config = JSON.parse(config);
        let model = new JSONModel(config);
        this.setModel(model, "config");
      } else {
        config = {
          col: {}
        };
        let model = new JSONModel(config);
        this.setModel(model, "config");
        let value = JSON.stringify(config);
        this.ls.setItem("config", value);
      }
    },
    popEditor: function() {
      let template = {
        all: {
          editable: false,
        },
        //indi: []
      };
      /*for (var i = 0; i < 150; i++) {
        template.indi.push({
          editable: false
        })
      }*/
      let oModel = new JSONModel(template);
      this.setModel(oModel, "state");
    },
    popConst: function() {
      let template = {
        operators: [
          {
            name: "Between",
            id: "BT",
            path: "sap.ui.model.FilterOperator.BT"
          },
          {
            name: "Contains",
            id: "Contains",
            path: "sap.ui.model.FilterOperator.Contains"
          },
          {
            name: "Ends With",
            id: "EndsWith",
            path: "sap.ui.model.FilterOperator.EndsWith"
          },
          {
            name: "Equals",
            id: "EQ",
            path: "sap.ui.model.FilterOperator.EQ"
          },
          {
            name: "Greater Than or Equals",
            id: "GE",
            path: "sap.ui.model.FilterOperator.GE"
          },
          {
            name: "Greater Than",
            id: "GT",
            path: "sap.ui.model.FilterOperator.GT"
          },
          {
            name: "Less Than or Equals",
            id: "BT",
            path: "sap.ui.model.FilterOperator.LE"
          },
          {
            name: "Less Than",
            id: "BT",
            path: "sap.ui.model.FilterOperator.LT"
          },
          {
            name: "Not Between",
            id: "NB",
            path: "sap.ui.model.FilterOperator.NB"
          },
          {
            name: "Not Equals",
            id: "NE",
            path: "sap.ui.model.FilterOperator.NE"
          },
          {
            name: "Not Contains",
            id: "NotContains",
            path: "sap.ui.model.FilterOperator.NotContains"
          },
          {
            name: "Not Ends With",
            id: "NotEndsWith",
            path: "sap.ui.model.FilterOperator.NotEndsWith"
          },
          {
            name: "Not Starts With",
            id: "NotStartsWith",
            path: "sap.ui.model.FilterOperator.NotStartsWith"
          },
          {
            name: "Starts With",
            id: "StartsWith",
            path: "sap.ui.model.FilterOperator.StartsWith"
          }
        ],
        bOperators: [
          {
            name: "Descending",
            id: true,
            path: true
          },
          {
            name: "Ascending",
            id: false,
            path: false
          }
        ]
      }
      let oModel = new JSONModel(template);
      this.setModel(oModel, "const");
    },
    dataSync: function() {
      let model = this.getModel("config");
      if (model) {
        let data = model.getData();
        let value = JSON.stringify(data);
        this.ls.setItem("config", value);
      }
      this.popTable();
    },
    popModel: function(data) {
      this.setModel(new JSONModel(data), "config");
      this.dataSync();
    },
    popControl: function() {
      let oButton = this.byId("oBtnCOL");
      let oList = this.byId("oLstCOL");
      oButton.setEnabled(true);
      oList.setNoDataText(this.i18n("CreateACollation"));
    },
    popMeta: function() {
      let oModel = this.getModel(this.model);
      oModel.getMetaModel().requestData().then(d => {
        console.log(d);
        let keys = Object.keys(d);
        let exp = /[A-Za-z0-9\.\_\-]+\.([A-Za-z0-9\_\-]+$)/;
        let list = [];
        for (let key of keys) {
          let value = d[key];
          if (value && value.$kind === "EntityType") {
            let name = exp.exec(key);
            if (name && name[1]) {
              list.push({
                name: name[1],
                value: value
              });
            }
          }
        }
        let meta = this.getModel("meta");
        meta.setProperty("/list", list);
        meta.setProperty("/filter", "");
      });
    },
    /* Obsolete */
    _getColumns: function(data) {
      return new Promise(resolve => {
        let columns = [];
        let iterable = Object.keys(data.value);
        this.table.columns = [];
        for (let column of iterable) {
          let exp = /(^[^\$][A-Za-z0-9\_\-]+$)/g;
          let valid = exp.test(column);
          if (valid) {
            this.table.columns.push(column);
            columns.push(new sap.m.Column({
              header: new sap.m.HBox({
                items: [
                  new sap.m.Text({
                    text: column,
                    maxLines: 1
                  })
                ]
              })
            }))
          }
        }
        resolve(columns);
      })
    },
    fnClippy: async function() {
      if (this._oClippy) this._oClippy.destroy();
      this._oClippy = await FragmentDefinition.getClippy("_oClippy", this);
      this.getView().addDependent(this._oClippy);
      this._oClippy.open();
    },
    openAdvancedFeatureReference: function() {
      window.open(`./resource/reference.pdf`, "_blank");
    },
    helpImport: function(index) {
      console.log(index);
      let template = {
        0: {
          title: "{i18n>SafelyAppend}",
          message: "{i18n>SafelyAppendMessage}",
          callback: (oEvent) => this.fnImport(oEvent)
        },
        1: {
          title: "{i18n>OverwriteDuplicates}",
          message: "{i18n>OverwriteDuplicatesMessage}",
          callback: (oEvent) => this.fnOverwrite(oEvent),
          additional_fields: [{
            label: "{i18n>PrimaryKeys}",
            control: () => {
              let control = new sap.m.MultiComboBox({
                id: this.createId("oMultiInput"),
                showValueHelp: false,
                items: this.table.columns.map(e => new sap.ui.core.Item({
                  key: e.name,
                  text: e.name
                }))
              });
              return control;
            },
          }]
        }
      }
      return template[index];
    },
    fnOnDataReceivedforDelete: function(oBinding, oFilter) {
      return new Promise(resolve => {
        let contexts = oBinding.getContexts();
        if (contexts.length > 0 && !this._OWCNCL) {
          let jobs = [];
          for (let context of contexts) {
            jobs.push(context.delete("$auto"));
          }
          this.fnSubmitBatch().then(() => {
            return Promise.allSettled(jobs);
          }).then(results => {
            for (let result of results) {
              if (result.status === "rejected") console.error(result.reason);
            }
            oBinding.filter(oFilter);
            return this.fnSubmitBatch();
          });
        } else {
          resolve();
        }
      })
    },
    fnOverwrite: function(oEvent) {
      return new Promise((resolve, reject) => {
        this._OWCNCL = false;
        let oBusyDialog = this.fnShowBusyDialog({
          title: this.i18n("Deleting"),
          text: this.i18n("ThisMayTakeSomeTime"),
          close: () => this._OWCNCL = true
        });
        let oFileUpload = this.byId("oFileUpload");
        let oMultiInput = this.byId("oMultiInput");
        let primary_keys = oMultiInput.getSelectedKeys();
        this.setBusy(true);
        let oSource = oFileUpload;
        let iImport = new Import(oSource, this);
        var table = this.getView().byId("oTbl");
        let oBinding = table.getBinding("rows");
        if (this._OWCNCL) return;
        iImport.filter(primary_keys).then(oFilter => {
          if (oFilter && !this._OWCNCL) {
            oBinding.filter(oFilter);
            let callback;
            callback = () => {
              this.fnOnDataReceivedforDelete(oBinding, oFilter).then(() => {
                oBusyDialog.close();
                oBinding.detachDataReceived(callback);
                return this.proxyImport(iImport);
              });
            }
            oBinding.attachDataReceived(callback);
          }
        }).catch(error => {
          reject(error);
        });
      })
    },
    btnImport: function(oEvent, index) {
      try {
        let template = this.helpImport(index);
        let content = [
          new sap.ui.core.Title({
            text: template.title,
          }),
          new sap.m.Label(),
          new sap.m.MessageStrip({
            text: template.message,
            type: "Warning",
            showCloseButton: false,
            showIcon: true,
          }),
          new sap.m.Label({
            text: "{i18n>Upload}"
          }),
          new sap.ui.commons.FileUploader({
            id: this.createId("oFileUpload"),
            multiple: false,
            fileType: ["xlsx","xls"],
            buttonText: "{i18n>Browse}",
            icon: "sap-icon://browse-folder",
          })
        ];
        if (template.additional_fields) {
          template.additional_fields.forEach(additional_field => {
            content = content.concat([
              new sap.m.Label({
                text: additional_field.label,
              }),
              additional_field.control()
            ]);
          });
        }
        let oDialog = new sap.m.Dialog({
          draggable: true,
          icon: "sap-icon://provision",
          title: "{i18n>Import}",
          content: [
            new sap.ui.layout.form.SimpleForm({
              width: "400px",
              maxContainerCols: 1,
              content: content
            })
          ],
          beginButton: new sap.m.Button({
            text: "{i18n>Close}",
            icon: "sap-icon://inspect-down",
            press: () => {
              oDialog.close();
              oDialog.destroy();
            }
          }),
          endButton: new sap.m.Button({
            text: "{i18n>Upload}",
            icon: "sap-icon://upload",
            press: template.callback
          })
        });
        this.getView().addDependent(oDialog);
        oDialog.open();
      } catch (error) {
        console.error(error);
      }
    },
    proxyImport: function(iImport) {
      return new Promise((resolve, reject) => {
        this.setBusy(true);
        let oFileUpload = this.byId("oFileUpload");
        let oSource = oFileUpload;
        let aError = [];
        var table = this.getView().byId("oTbl");
        let oBinding = table.getBinding("rows");
        let progress = new Progress(this, this.i18n("Importing"), this.i18n("PRT01"));
        progress.open();
        iImport.xlsx().then(async data => {
          let blocks = data.blocks;
          aError = data.aError;
          for (var i = 0; i < blocks.length; i++) {
            if (progress.getCancelled()) break;
            progress.setText(this.i18n("PRT02"));
            progress.setPercentValue(Math.floor((i/blocks.length) * 100));
            let rows = blocks[i];
            await Promise.allSettled(rows.map(row => {
              let f = oBinding.create(row);
              this.fnSubmitBatch();
              return f.created();
            })).then((results) => {
              results.forEach(result => {
                if (result.status === "rejected") aError.push(result.reason);
              });
            }).catch(error => {
              aError.push(error);
            });
          }
        }).catch(error => {
          reject(error)
        }).finally(() => {
          progress.close();
          aError.forEach(e => {
            this.newMessage({
              message: this.i18n("importErr"),
              description: e,
              type: "Error",
            });
          });
          oSource.clear();
          this.setBusy(false);
          resolve();
        })
      });
    },
    fnImport: function(oEvent) {
      return new Promise((resolve, reject) => {
        let oFileUpload = this.byId("oFileUpload");
        let oSource = oFileUpload;
        let iImport = new Import(oSource, this);
        return this.proxyImport(iImport);
      })
    },
    genTemplate: function(oEvent) {
      let iImport = new Import(null, this);
      iImport.template();
    },
    fnShowBusyDialog(opts) {
      let oBusyDialog = new sap.m.BusyDialog({
        title: opts.title,
        text: opts.text,
        showCancelButton: true,
        close: opts.close
      });
      oBusyDialog.open();
      return oBusyDialog;
    },
    fnAllRows: function(oEvent) {
      let cancel = false;
      let oBusyDialog = this.fnShowBusyDialog({
        title: this.i18n("Downloading"),
        text: this.i18n("ThisMayTakeSomeTime"),
        close: () => cancel = true
      });
      let iImport = new Import(null, this);
      let table = this.byId("oTbl");
      let sOrigin = window.location.origin;
      let sServiceUrl = this.getModel(this.model).sServiceUrl;
      let sPath = table.getBinding("rows").sPath;
      if (cancel) return;
      fetch(`${sOrigin}${sServiceUrl}${sPath}`).then(response => {
        if (!response.ok) throw new Error(response.statusText);
        if (!cancel) return response.json();
      }).then((data) => {
        if (!cancel) return iImport.download(data.value);
      }).then(() => {
        oBusyDialog.close();
      }).catch(error => {
        this.newMessage({
          message: this.i18n("EVERYROWDLMessage"),
          description: error,
          type: "Error",
        });
      });
    },
    fnCheckedOnly: function(oEvent) {
      let cancel = false;
      let oBusyDialog = this.fnShowBusyDialog({
        title: this.i18n("Downloading"),
        text: this.i18n("ThisMayTakeSomeTime"),
        close: () => cancel = true
      });
      let iImport = new Import(null, this);
      let table = this.byId("oTbl");
      let selectedIndicies = table.getSelectedIndices();
      let oData = selectedIndicies.map(e => table.getContextByIndex(e).getObject());
      if (cancel) return;
      iImport.download(oData).then(() => {
        oBusyDialog.close();
      });
    },
    fnMenuItem: function(oEvent, mode) {
      let oParameters = oEvent.getParameters();
      let column = oParameters.item.getParent().getParent();
      let table = this.byId("oTbl");
      let name = column.getLabel().getText();
      let bDescending = sap.ui.table.SortOrder.Descending === mode;
      console.log(`/${name}`, bDescending, true);
      let oSorter = sap.ui.model.Sorter(name, bDescending, true);
      column.setSortOrder(mode);
      column.setSorted(true);
      table.sort([oSorter]);
    },
    fnMenuTextFieldItem: function(oEvent, mode) {
      console.log(oEvent.getParameters());
    },
    popCell: function(type, model, column) {
      return new Promise(async (resolve, reject) => {
        let has = this.EDM.has(type);
        let isMatch = await this.appValueHelp.isMatch(this.table.name, column).catch(err => console.error(err));
        if (isMatch) {
          resolve(this.appValueHelp.getControl(this.table.name, column, `${model}>${column}`, "state>/all/editable", isMatch));
        } else if (has) {
          let setting = this.EDM.get(type)();
          var properties = setting.template;
          properties[setting.prop].path = `${model}>${column}`;
          properties.editable = "{state>/all/editable}";
          properties.visible = "{state>/all/editable}";
          resolve(new setting.control(properties));
        } else {
          resolve(new sap.m.Input({
            value: `{${model}>${column}}`,
            editable: "{state>/all/editable}",
            visible: "{state>/all/editable}"
          }));
        }
      })
    },
    getColumns: function(data) {
      return new Promise(async resolve => {
        let columns = [];
        console.log(data);
        let iterable = Object.keys(data.value);
        this.table.columns = [];
        for (let column of iterable) {
          let exp = /(^[^\$][A-Za-z0-9\_\-]+$)/g;
          let valid = exp.test(column);
          let type = data.value[column].$Type;
          if (valid) {
            this.table.columns.push({
              name: column,
              setting: {
                type: type,
                nullable: data.value[column].$Nullable,
                maxLength: data.value[column].$MaxLength,
              }
            });
            columns.push(new sap.ui.table.Column({
              /*menu: new sap.ui.unified.Menu({
                items: [
                  new sap.ui.unified.MenuItem({
                    text: this.i18n("SortDescending"),
                    icon: "sap-icon://sort-descending",
                    select: (e) => this.fnMenuItem(e, sap.ui.table.SortOrder.Descending)
                  }),
                  new sap.ui.unified.MenuItem({
                    text: this.i18n("SortAscending"),
                    icon: "sap-icon://sort-ascending",
                    select: (e) => this.fnMenuItem(e, sap.ui.table.SortOrder.Ascending)
                  }),
                  new sap.ui.unified.MenuTextFieldItem({
                    label: this.i18n("Filter"),
                    icon: "sap-icon://filter",
                    select: (e) => this.fnMenuTextFieldItem(e)
                  }),
                  new sap.ui.unified.MenuItem({
                    text: this.i18n("Instructions"),
                    icon: "sap-icon://sys-help",
                    select: (e) => this.fnClippy(e)
                  })
                ]
              }),*/
              label: new sap.m.Label({
                text: column
              }),
              template: new sap.m.FlexBox({
                items: [
                  new sap.m.Text({
                    text: `{${this.model}>${column}}`,
                    visible: "{= !${state>/all/editable} }",
                    maxLines: 1
                  }),
                  await this.popCell(type, this.model, column)
                ]
              })
            }))
          }
        }
        resolve(columns);
      })
    },
    fnBulk: function(oEvent) {
      let table = this.byId("oTbl");
      let oModel = this.getModel("state");
      oModel.setProperty("/all/editable", true);
      table.rerender();
    },
    fnView: function(oEvent) {
      this.fnSubmitBatch().then(() => {
        let oModel = this.getModel("state");
        oModel.setProperty("/all/editable", false);
      }).catch(error => {
        this.fatal(error);
      })
    },
    fnForget: function(oEvent) {
      this.fnResetChanges();
      let oModel = this.getModel("state");
      oModel.setProperty("/all/editable", false);
    },
    fnDel: function(oEvent) {
      let item = this.getModel("item").getData();
      let config = this.getModel("config").getData();
      let exp = /\/col\/([\w\d]+)\/([\d]+)/g;
      let match = exp.exec(item.path);
      if (match) {
        let table = match[1];
        let index = match[2];
        console.log(item.path, config, match);
        config.col[table].splice(index, 1);
        let oModel = new JSONModel(config);
        this.setModel(oModel, "config");
        this.dataSync();
      } else {
        this.newMessage({
          message: this.i18n("COLDELERRMessage"),
          description: this.i18n("COLDELERRDescription"),
          type: "Error",
        });
      }
      this.fnDestroy("_COL1");
    },
    popTemp: async function() {
      let model = {}
      this.table.columns.forEach(column => {
        let hide = ["createdBy", "createdAt", "modifiedBy", "modifiedAt"];
        if (!hide.includes(column.name)) model[column.name] = null;
      });
      let oModel = new JSONModel(model);
      this.setModel(oModel, "insert");
    },
    copy: function(data) {
      return new Promise(resolve => {
        function dig(data) {
          let value;
          let key;
          if (typeof data != Object) resolve(data);
          let response = Array.isArray(data) ? [] : {};
          for (key in data) {
            value = data[key];
            response[key] = dig(value);
          }
          return response;
        }
        let response = dig(data);
        resolve(response);
      })
      return JSON.parse(JSON.stringify(data));
    },
    popField: function(type, model, column) {
      return new Promise(async resolve => {
        let has = this.EDM.has(type);
        let isMatch = await this.appValueHelp.isMatch(this.table.name, column).catch(err => reject(err));
        if (isMatch) {
          resolve(this.appValueHelp.getControl(this.table.name, column, `${model}>${column}`));
        } else if (has) {
          let setting = this.EDM.get(type)();
          let properties = setting.template;
          properties[setting.prop].path = `${model}>${column}`;
          let control = new setting.control(properties);
          resolve(control);
        } else {
          resolve(new sap.m.Input({
            value: `{${model}>${column}}`
          }));
        }
      })
    },
    onOperatorChange: function(oEvent) {
      let oSource = oEvent.getSource();
      let oSimpleForm = oSource.getParent().getParent().getParent().getParent();
      let oParameters = oEvent.getParameters();
      let selectedItem = oParameters.selectedItem;
      selectedItem = selectedItem.getBindingContext("const").getObject();
      if (selectedItem) {
        oSimpleForm.getContent()[7].setEnabled(["Not Between","Between"].includes(selectedItem.name));
      }
    },
    onFilterCol: async function(oEvent) {
      let selectedItem;
      let oSource;
      if (oEvent.getParameters) {
        let oParameters = oEvent.getParameters();
        oSource = oEvent.getSource();
        selectedItem = oParameters.selectedItem;
      } else if (oEvent.srcControl) {
        oSource = oEvent.srcControl;
        selectedItem = oSource.getItems().find(e => e.getBindingContext("table").getObject().name === oSource.getValue());
      }
      console.log(selectedItem);
      if (selectedItem) {
        selectedItem = selectedItem.getBindingContext("table").getObject();
        let form = this.getParent(oSource, 4);
        let indicies = [5,7];
        let fields = form.getContent().filter((e,i) => indicies.includes(i));
        for (var i = 0; i < fields.length; i++) {
          let control = await this.popField(selectedItem.setting.type, "item", `/value${i+1}`);
          console.log(form.getContent(), indicies[i]);
          if (!form.getContent()[indicies[i]].getMetadata()._sClassName || control.getMetadata()._sClassName === form.getContent()[indicies[i]].getMetadata()._sClassName) break;
          form.removeContent(indicies[i]);
          form.insertContent(control, indicies[i]);
        }
      }
    },
    getParent(control, index) {
      for (var i = 0; i < index; i++) {
        control = control.getParent();
      }
      return control;
    },
    popForm: function(model) {
      return new Promise(async resolve => {
        model = model ? model : this.model;
        let template = {
          content: []
        }
        for (let column of this.table.columns) {
          let hide = ["createdBy", "createdAt", "modifiedBy", "modifiedAt"];
          if (!hide.includes(column.name)) {
            let field = await this.popField(column.setting.type, model, `/${column.name}`);
            template.content.push(new sap.m.Label({
              text: column.name
            }));
            template.content.push(field)
          }
        };
        let form = new sap.ui.layout.form.SimpleForm(template);
        console.log(form);
        resolve(form);
      })
    },
    fnRefresh: function() {
      var table = this.getView().byId("oTbl");
      table.refreshRows();
    },
    fnCreate: async function(oEvent) {
      if (this._CRT1) this._CRT1.destroy();
      this.popTemp();
      let form = await this.popForm("insert");
      console.log(form);
      let buttons = [
        new sap.m.Button({
          text: this.i18n("Create"),
          type: "Emphasized",
          press: (e) => this.opCreate(e).then((s) => {
            MessageToast.show(s)
            this.fnDestroy("_CRT1")
          }).catch(error => MessageToast.show(error))
        }),
        new sap.m.Button({
          text: this.i18n("Cancel"),
          press: (e) => this.fnDestroy("_CRT1")
        })
      ];
      this._CRT1 = new sap.m.Dialog({
        customHeader: new sap.m.OverflowToolbar({
          content: [
            new sap.m.Title({
              text: this.i18n("Create")
            }),
            new sap.m.ToolbarSpacer,
            new sap.m.Button({
              icon: "sap-icon://alert",
              text: "{= ${message>/}.length }",
              visible: "{= ${message>/}.length > 0 }",
              type: "Emphasized",
              press: (e) => this.onMessagePopoverPress(e)
            })
          ]
        }),
        content: [form],
        buttons: buttons
      });
      this.getView().addDependent(this._CRT1);
      this._CRT1.open();
    },
    opCreate: function(oEvent) {
      return new Promise((resolve, reject) => {
        let source = oEvent.getSource().getParent();
        source.setBusyIndicatorDelay(0);
        source.setBusy(true);
        var table = this.getView().byId("oTbl");
        let oBinding = table.getBinding("rows");
        let data = this.getModel("insert").getData();
        let oContext = oBinding.create(data);
        this.fnSubmitBatch();
        oContext.created().then(() => {
          resolve(this.i18n("sEntity"));
        }, (error) => {
          throw error;
        }).catch(error => {
          this.fnResetChanges();
          reject(error);
        }).finally(() => {
          source.setBusy(false);
        });
      })
    },
    getItems: function(data) {
      return new Promise(resolve => {
        let cells = [];
        let iterable = Object.keys(data.value);
        for (let column of iterable) {
          let exp = /(^[^\$][A-Za-z0-9\_\-]+$)/g;
          let valid = exp.test(column);
          if (valid) {
            cells.push(new sap.m.Text({
              text: `{${this.model}>${column}}`
            }))
          }
        }
        resolve(new sap.m.ColumnListItem({
          cells: cells
        }))
      })
    },
    getCol: function(data) {

    },
    safeBinding: function(oEvent) {
      let table = this.byId("oTbl");
      let oParameters = oEvent.getParameters();
      if (oParameters.error) {
        this.fatal(new Error(oParameters.error.message), {
          actions: [this.i18n("Retry")]
        }).then((oAction) => {
          table.setNoData(oParameters.error.message);
          if (oAction === "Retry") table.rerender();
        });
      } else {
        table.setNoData(this.i18n("NoData"));
      }
      table.setBusy(false);
    },
    getTemplate: function(e) {
      if (e === "Filter") {
        return {
          name: null,
          operator: null,
          column: null,
          value1: null,
          value2: null,
          enabled: true,
          type: "Filter"
        }
      } else if (e === "Sorter") {
        return {
          name: null,
          operator: null,
          column: null,
          enabled: true,
          type: "Sorter"
        }
      }
    },
    onPressMenu: async function(oEvent) {
      let parameters = oEvent.getParameters();
      let item = parameters.item.getText();
      let template = this.getTemplate(item);
      if (this._MNU1) this._MNU1.destroy();
      if (item === "Filter") {
        this._MNU1 = await FragmentDefinition.getFilter("_MNU1", this);
      } else if (item === "Sorter") {
        this._MNU1 = await FragmentDefinition.getSorter("_MNU1", this);
      }
      this.setModel(new JSONModel(template), "item");
      this.getView().addDependent(this._MNU1);
      this._MNU1.open();

    },
    onSaveCol: async function() {
      let oModel = this.getModel("config");
      let tModel = this.getModel("item");
      let data = tModel.getData();
      let path = data.path;
      delete data.path;
      oModel.setProperty(path, data);
      this.dataSync();
      this.fnDestroy("_COL1");
    },
    onPressCol: async function(oEvent) {
      let parameters = oEvent.getParameters();
      let item = parameters.listItem.getBindingContext("config");
      let data = item.getObject();
      data.path = item.getPath();
      if (this._COL1) this._COL1.destroy();
      if (data.type === "Filter") {
        this._COL1 = await FragmentDefinition.getFilter("_COL1", this, true);
      } else if (data.type === "Sorter") {
        this._COL1 = await FragmentDefinition.getSorter("_COL1", this, true);
      }
      this.setModel(new JSONModel(data), "item");
      this.getView().addDependent(this._COL1);
      this._COL1.open();
      parameters.listItem.setSelected(false);
    },
    onCreateCol: function(oEvent) {
      let tModel = this.getModel("item");
      let item = tModel.getData();
      let oModel = this.getModel("config");
      let data = oModel.getData();
      if (data.col[this.table.name]) {
        data.col[this.table.name].push(item);
      } else {
        data.col[this.table.name] = [item];
      }
      this.popModel(data);
      this.setModel(null, "item");
    },
    bCols: function() {
      let list = this.byId("oLstCOL");
      list.bindAggregation("items", {
        path: `config>/col/${this.table.name}`,
        templateShareable: true,
        template: new sap.m.CustomListItem({
          content: [
            new sap.m.HBox({
              justifyContent: "SpaceBetween",
              alignItems: "Center",
              items: [
                new sap.m.HBox({
                  alignItems: "Center",
                  items: [
                    new sap.m.Title({
                      text: "{config>column} {config>operator} {config>value1} {config>value2}"
                    })
                  ]
                }),
                new sap.m.HBox({
                  alignItems: "Center",
                  items: [
                    new sap.m.Switch({
                      type: "AcceptReject",
                      state: "{config>enabled}",
                      change: (e) => this.dataSync(e)
                    })
                  ]
                })
              ]
            }).addStyleClass("sapUiSmallMarginBeginEnd")
          ]
        })
      });
    },
    getSorters: function() {
      return new Promise((resolve, reject) => {
        let oModel = this.getModel("config");
        let oSorters = [];
        let data = oModel.getProperty(`/col/${this.table.name}`);
        if (data) {
          let items = data.filter(e => e.type === "Sorter" && e.enabled);
          for (const item of items) {
            if (item.column && item.operator) {
              let bDescending = this.bDescending.get(item.operator);
              oSorters.push(new sap.ui.model.Sorter(item.column, bDescending, true));
            }
          }
        }
        resolve(oSorters);
      })
    },
    getFilters: function() {
      return new Promise((resolve, reject) => {
        let oModel = this.getModel("config");
        let oFilters = [];
        let data = oModel.getProperty(`/col/${this.table.name}`);
        if (data) {
          let items = data.filter(e => e.type === "Filter" && e.enabled);
          for (const item of items) {
            if (item.column, item.operator) {
              let oOperator = this.FilterOperator.get(item.operator);
              oFilters.push(new sap.ui.model.Filter(item.column, oOperator, item.value1, item.value2));
            }
          }
        }
        resolve(oFilters);
      })
    },
    dEnable: function() {
      let buttons = ["oBtnRefresh", "oBtnInsert", "oBtnBulk", "oBtnAudit", "oMenu", "oBtnQuickQuery"];
      buttons.forEach(e => this.byId(e).setEnabled(true));
    },
    sEnable: function(oEvent) {
      let table = oEvent.getSource();
      let selectedIndicies = table.getSelectedIndices();
      let buttons = ["oBtnDel"];
      if (selectedIndicies.length > 0) {
        buttons.forEach(e => this.byId(e).setEnabled(true));
      } else {
        buttons.forEach(e => this.byId(e).setEnabled(false));
      }
    },
    oEnable: function() {
      let buttons = ["oBtnSave"];
      buttons.forEach(e => this.byId(e).setEnabled(true));
    },
    popTable: async function() {
      let table = this.byId("oTbl");
      table.setBusy(true);
      var oListBinding = table.getBinding("rows");
      let oFilters = await this.getFilters();
      let oSorters = await this.getSorters();
      oListBinding.filter(oFilters);
      oListBinding.sort(oSorters);
    },
    fnQuickRun: function(oEvent) {
      let oSource = oEvent.getSource();
      console.log(oSource);
      let meta = this.getModel("meta");
      let filter = meta.getProperty("/filter");
      if (filter.length > 0) {
        oSource.setValueState("Success");
        let table = this.byId("oTbl");
        table.setBusy(true);
        var oListBinding = table.getBinding("rows");
        table.bindAggregation("rows", {
          path: `${this.model}>${oListBinding.sPath}`,
          templateShareable: true,
          mode: "TwoWay",
          parameters: {
            $$operationMode: "Server",
            $$updateGroupId: "slowGroup",
            $filter: filter
          },
          events: {
            dataReceived: this._safeBinding.bind(this)
          }
        });
      } else {
        oSource.setValueState("None");
        this.defaultBind();
      }
    },
    defaultBind: async function() {
      let table = this.byId("oTbl");
      var oListBinding = table.getBinding("rows");
      table.bindAggregation("rows", {
        path: `${this.model}>${oListBinding.sPath}`,
        templateShareable: true,
        mode: "TwoWay",
        filter: await this.getFilters(),
        sorter: await this.getSorters(),
        parameters: {
          $$operationMode: "Server",
          $$updateGroupId: "slowGroup",
        },
        events: {
          dataReceived: this.safeBinding.bind(this)
        }
      });
    },
    _safeBinding: function(oEvent) {
      let table = this.byId("oTbl");
      let oParameters = oEvent.getParameters();
      if (oParameters.error) {
        this.fatal(new Error(oParameters.error.message), {
          actions: [this.i18n("Retry")]
        }).then((oAction) => {
          table.setNoData(oParameters.error.message);
          if (oAction === "Retry") {
            table.rerender();
          } else {
            this.defaultBind();
          }
        });
      } else {
        table.setNoData(this.i18n("NoData"));
      }
      table.setBusy(false);
    },

    tRowAction(row, b) {
      this._rOpen = !b;
      let cells = row.getCells();
      let action = row.getRowAction();
      var items = action.getItems();
      for (let item of items) {
        let def = item.data("default");
        item.setVisible(def === b.toString());
      }
      for (let cell of cells) {
        var items = cell.getItems();
        items[0].setVisible(b);
        items[1].setVisible(!b);
        items[1].setEditable(!b);
      }
      row.rerender();
    },
    fnBus: function(oEvent) {
      let parameters = oEvent.getParameters();
      let data = {
        parameters: parameters,
      }
      this.bus.publish("flexible", "popOverlay", data);
    },
    fnResetChanges: function(oEvent, groupId) {
      groupId = groupId ? groupId : "slowGroup";
      let oModel = this.getModel(this.model);
      oModel.resetChanges(groupId);
    },
    fnSubmitBatch: function(groupId) {
      return new Promise((resolve, reject) => {
        let table = this.byId("oTbl");
        groupId = groupId ? groupId : "slowGroup";
        let oModel = this.getModel(this.model);
        oModel.submitBatch(groupId).then(() => {
          this.getModel(this.model).refresh();
          resolve();
        }).catch(error => {
          oModel.resetChanges(groupId);
          reject(error);
        })
      })
    },
    singlDelete: function(oEvent) {
      let parameters = oEvent.getParameters();
      let row = parameters.row;
      let oBindingContext = row.getBindingContext(this.model);
      let callback = this.opSingleDel.bind(this, oBindingContext);
      this.popWarning(callback);
    },
    multiDelete: function() {
      let table = this.byId("oTbl");
      let selectedIndicies = table.getSelectedIndices();
      let oContexts = selectedIndicies.map(e => table.getContextByIndex(e));
      let callback1 = this.opMultiDel.bind(this, oContexts);
      let callback2 = this.popConfirm.bind(this, callback1);
      this.popWarning(callback2);
    },
    popWarning: function(callback) {
      MessageBox.warning(this.i18n("scaryDelete"), {
				actions: [MessageBox.Action.DELETE, MessageBox.Action.CANCEL],
				emphasizedAction: MessageBox.Action.CANCEL,
				onClose: function (sAction) {
					if (sAction === MessageBox.Action.DELETE) {
            callback();
          }
				}
			});
    },
    popConfirm: function(callback) {
      MessageBox.warning(this.i18n("confirmDelete"), {
				actions: [MessageBox.Action.DELETE, MessageBox.Action.CANCEL],
				emphasizedAction: MessageBox.Action.CANCEL,
				onClose: function (sAction) {
					if (sAction === MessageBox.Action.DELETE) {
            callback();
          }
				}
			});
    },
    opMultiDel: function(oContexts) {
      return new Promise((resolve, reject) => {
        this.setBusy(true);
        let jobs = oContexts.map(e => e.delete("$direct"));
        Promise.allSettled(jobs).then((results) => {
          results.forEach(result => {
            if (result.status === "rejected") {
              this.newMessage({
                message: this.i18n("multiDeleteErr"),
                description: result.reason,
                type: "Error",
              });
            }
          });
          resolve(this.i18n("multiEntity"));
        }).catch(error => {
          reject(error);
        }).finally(() => {
          this.setBusy(false);
        })
      })
    },
    opSingleDel: function(oBindingContext) {
      return new Promise((resolve, reject) => {
        this.setBusy(true);
        oBindingContext.delete("$direct").then(() => {
          resolve(this.i18n("dEntity"));
        }, (error) => {
          throw error;
        }).catch(error => {
          this.fnResetChanges();
          reject(error);
        }).finally(() => {
          this.setBusy(false);
        });
      })
    },
    _onSelectionChange: function(oEvent) {
      let table = this.byId("oTbl");
      table.setBusy(true);
      let oSource = oEvent.getSource();
      let oParameters = oEvent.getParameters();
      let listItem = oParameters.listItem;
      let data = listItem.getBindingContext("meta").getObject();
      this.table.name = data.name;
      table.removeAllColumns();
      this.getColumns(data).then(async columns => {
        columns.forEach(column => table.addColumn(column));
        this.setModel(new JSONModel(this.table), "table");
        return this.getItems(data);
      }).then(async items => {
        table.bindAggregation("items", {
          path: `${this.model}>/${data.name}`,
          templateShareable: true,
          template: items,
          filters: await this.getFilters(),
          sorters: await this.getSorters(),
          mode: "TwoWay",
          parameters: {
            $$operationMode: "Server",
            $$updateGroupId: "slowGroup",
          },
          events: {
            dataReceived: this.safeBinding.bind(this)
          }
        });
        this.bCols();
      }).finally(() => {
        this.popControl();
        //this._oMM1.close();
      })
    },
    onSelectionChange: function(oEvent) {
      let table = this.byId("oTbl");
      table.setBusy(true);
      let oSource = oEvent.getSource();
      let oParameters = oEvent.getParameters();
      let listItem = oParameters.listItem;
      let data = listItem.getBindingContext("meta").getObject();
      this.table.name = data.name;
      table.removeAllColumns();
      this.getColumns(data).then(async columns => {
        this.setModel(new JSONModel(this.table), "table");
        columns.forEach(column => table.addColumn(column));
        table.bindAggregation("rows", {
          path: `${this.model}>/${data.name}`,
          templateShareable: true,
          filters: await this.getFilters(),
          sorters: await this.getSorters(),
          mode: "TwoWay",
          parameters: {
            $$operationMode: "Server",
            $$updateGroupId: "slowGroup"
          },
          events: {
            dataReceived: this.safeBinding.bind(this)
          }
        });
      }).finally(() => {
        this.bCols();
        this.popControl();
        this.bindAudit();
        this.dEnable();
        //this._oMM1.close();
      })
    },
    onSelect: function(oEvent) {
      let oSource = oEvent.getSource();
      if (!this._oMM1) {
				this._oMM1 = new sap.m.Popover({
          title: this.i18n("SelectTable"),
          placement: "Bottom",
          content: new sap.m.List({
            mode: "SingleSelectMaster",
            selectionChange: (oEvent) => this.onSelectionChange(oEvent),
            items: {
              path: "meta>/list",
              template: new sap.m.StandardListItem({
                title: "{meta>name}"
              })
            }
          })
        })
        this.getView().addDependent(this._oMM1);
        this._oMM1.openBy(oSource);
			} else {
				this._oMM1.openBy(oSource);
			}
    }
  });
});
