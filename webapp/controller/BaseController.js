sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/core/routing/History",
  "sap/ui/core/UIComponent",
  "com/ventia/security_app/model/formatter",
  "sap/m/MessageBox",
  "com/ventia/security_app/common/fragment.definition",
  "sap/ui/core/message/Message"
], function(Controller, History, UIComponent, formatter, MessageBox, FragmentDefinition, Message) {
  "use strict";
  
  return Controller.extend("com.ventia.security_app.controller.BaseController", {
    
    formatter: formatter,
    onInit: async function() {
      let model = this.getModel("default");
      let oMetaModel = model.getMetaModel();
      oMetaModel.oRequestor.read(oMetaModel.sUrl).catch(error => {
        this.tagChange(this.i18n("NotConnected"), "Error");
        this.newMessage({
          message: this.i18n("ConnectionError"),
          description: error.message,
          type: "Error",
        });
      });
      let oView = this.getView();
      this.setup(oView);
      if (this._onInit) this._onInit();
    },
    /**
    * Convenience method for getting the view model by name in every controller of the application.
    * @public
    * @param {string} sName the model name
    * @returns {sap.ui.model.Model} the model instance
    */
    getModel: function(sName) {
      return this.getView().getModel(sName);
    },
    
    /**
    * Convenience method for setting the view model in every controller of the application.
    * @public
    * @param {sap.ui.model.Model} oModel the model instance
    * @param {string} sName the model name
    * @returns {sap.ui.mvc.View} the view instance
    */
    setModel: function(oModel, sName) {
      return this.getView().setModel(oModel, sName);
    },
    
    /**
    * Convenience method for getting the resource bundle.
    * @public
    * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
    */
    getResourceBundle: function() {
      return this.getOwnerComponent().getModel("i18n").getResourceBundle();
    },
    
    /**
    * Method for navigation to specific view
    * @public
    * @param {string} psTarget Parameter containing the string for the target navigation
    * @param {mapping} pmParameters? Parameters for navigation
    * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
    */
    navTo: function(psTarget, pmParameters, pbReplace) {
      this.getRouter().navTo(psTarget, pmParameters, pbReplace);
    },
    
    getRouter: function() {
      return UIComponent.getRouterFor(this);
    },
    setBusy(bBusy) {
      let oView = this.getView();
      oView.setBusy(bBusy);
    },
    onNavBack: function() {
      var sPreviousHash = History.getInstance().getPreviousHash();
      
      if (sPreviousHash !== undefined) {
        window.history.back();
      } else {
        this.getRouter().navTo("appHome", {}, true /*no history*/ );
      }
    },
    i18n: function(query) {
      return this.getResourceBundle().getText(query);
    },
    tagChange: function(text, status) {
      let oGenericTag = this.byId("genericTagId");
      oGenericTag.setText(text);
      oGenericTag.setStatus(status);
    },
    fnDestroy: function(name) {
      if (this[name]) {
        this[name].close();
        this[name].destroy();
        this[name] = null;
      }
    },
    setup: function(oView) {
      let oMessageManager = sap.ui.getCore().getMessageManager();
      oView.setModel(oMessageManager.getMessageModel(), "message");
      oMessageManager.registerObject(oView, true);
      oView.setBusyIndicatorDelay(0);
    },
    fatal: function(error, opts) {
      return new Promise(resolve => {
        if (!this.byId("fatal")) {
          let template = {
            id: this.createId("fatal"),
            icon: MessageBox.Icon.ERROR,
            title: error.name ? error.name : this.i18n("Error"),
            actions: [MessageBox.Action.OK],
            emphasizedAction: MessageBox.Action.OK,
            onClose: (oAction) => {
              resolve(oAction);
            }
          }
          if (opts) {
            if (opts.actions) template.actions = template.actions.concat(opts.actions);
          }
          MessageBox.show(
            `${error.message ? error.message : error}`, template)
          }
          //eslint-disable-next-line
          console.error(error);
        })
      },
      onMessagePopoverPress : function (oEvent) {
        let oSource = oEvent.getSource();
        if (!this._Messages) {
          FragmentDefinition.getMessage(this).then(function(oMessageManager){
            this._Messages = oMessageManager;
            this.getView().addDependent(this._Messages);
            this._Messages.openBy(oSource);
          }.bind(this));
        } else {
          return this._Messages.openBy(oSource);
        }
      },
      onClearPress : function(){
        sap.ui.getCore().getMessageManager().removeAllMessages();
      },
      newMessage: function(data) {
        var oMessage = new Message(data);
        sap.ui.getCore().getMessageManager().addMessages(oMessage);
      },
    });
    
  });
  