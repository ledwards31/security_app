
sap.ui.define([
    'sap/ui/commons/Button',
    'sap/ui/core/Fragment'
], function(
    Button,
    Fragment
) {
    "use strict";
    class AppValueHelp {
        constructor(sModel, sReference) {
            this.model = sModel;
            this.reference = sReference ? sReference : "AppValueHelp";
            this.map = new Map();
        }
        request(table, field) {
            return new Promise((resolve, reject) => {
                let origin = window.origin;
                let has = this.map.get(table);
                if (!has) {
                    fetch(`${origin}${this.model}${this.reference}?$filter=Usage eq '${table}'`, {
                        method: 'GET',
                        cache: 'no-cache',
                        credentials: 'same-origin',
                        headers: {
                            'Accept': '*/*',
                            'Accept-Encoding': 'gzip, deflate, br',
                            'Connection': 'keep-alive',
                            'Accept-Language': 'en-GB',
                        }
                    }).then(response => {
                        if (!response.ok) {
                            reject(response.statusText);
                        } else {
                            return response.json();
                        }
                    }).then(json => {
                        let data = json.value;
                        this.map.set(table, data);
                        resolve(data.filter(e => e.Object === field));
                    }).catch(error => {
                        reject(error);
                    })
                } else {
                    let data = this.map.get(table);
                    resolve(data.filter(e => e.Object === field))
                }
            })
        }
        isMatch(sTable, sField) {
            return new Promise((resolve, reject) => {
                let table = sTable;
                let field = sField.indexOf("/") === 0 ? sField.split("/")[1] : sField;
                this.request(table, field).then(data => {
                    resolve(data.length > 0 ? data : false);
                }).catch(error => {
                    console.error(error);
                    resolve(false);
                })
            })
        }
        getControl(sTable, sField, sPath, sVisiedit, oData) {
            return new Promise(async (resolve, reject) => {
                let table = sTable;
                let field = sField.indexOf("/") === 0 ? sField.split("/")[1] : sField;
                let data = oData ? oData : await this.request(table, field).catch(error => reject(error));
                if (data.length > 0) {
                    resolve(new sap.m.ComboBox({
                        showSecondaryValues: true,
                        filterSecondaryValues: true,
                        value: `{${sPath}}`,
                        visible: `{${sVisiedit}}`,
                        editable: `{${sVisiedit}}`,
                        items: data.map(e => new sap.ui.core.ListItem({
                            key: e.Field,
                            text: e.Field,
                            additionalText: e.Description
                        }))
                    }))
                } else {
                    resolve();
                }
            })
        }
    }
    return AppValueHelp;
});