sap.ui.define([
    'sap/ui/commons/Button',
    'sap/ui/core/Fragment'
], function(
    Button,
    Fragment
) {
    "use strict";
    let module = {
      getMessage: (oController) => {
        return new Promise(resolve => {
            let control = new sap.m.MessagePopover({
                items: {
                    path: "message>/",
                    template: new sap.m.MessagePopoverItem({
                        type: "{message>type}",
                        title: "{message>message}",
                        subtitle: "{message>additionalText}",
                        description: "{message>description}",
                    })
                },
                initiallyExpanded: true,
                afterClose: ($event) => {
                    oController.onClearPress($event);
                }
            });
            resolve(control);
        })
      },
      getFilter: (name, controller, tx) => {
        return new Promise(resolve => {
          let buttons = [
            new sap.m.Button({
              text: "{i18n>Save}",
              type: "Accept",
              press: (e) => {
                tx ? controller.onSaveCol(e) : controller.onCreateCol(e);
                controller.fnDestroy(name);
              }
            }),
            new sap.m.Button({
              text: "{i18n>Cancel}",
              type: "Default",
              press: (e) => controller.fnDestroy(name)
            })
          ]
          let content = [
            new sap.m.Label({
              text: "{i18n>Column}"
            }),
            new sap.m.ComboBox({
              items: {
                path: "table>/columns",
                template: new sap.ui.core.Item({
                  key: "{table>name}",
                  text: "{table>name}"
                })
              },
              selectionChange: (e) => controller.onFilterCol(e),
              value: "{item>/column}"
            }).addEventDelegate({
              "onAfterRendering": (e) => controller.onFilterCol(e)
            }, this),
            new sap.m.Label({
              text: "{i18n>Operator}"
            }),
            new sap.m.ComboBox({
              items: {
                path: "const>/operators",
                template: new sap.ui.core.Item({
                  key: "{const>name}",
                  text: "{const>name}"
                })
              },
              selectionChange: (e) => controller.onOperatorChange(e),
              value: "{item>/operator}"
            }),
            new sap.m.Label({
              text: "{i18n>Value1}"
            }),
            new sap.m.Input({
              value: "{item>/value1}"
            }),
            new sap.m.Label({
              text: "{i18n>Value2}"
            }),
            new sap.m.Input({
              value: "{item>/value2}"
            }),
            new sap.m.Label(),
          ];
          if (tx) content.push(new sap.m.Button({
            text: "{i18n>Delete}",
            type: "Reject",
            press: (e) => controller.fnDel(e)
          }));
          let control = new sap.m.Dialog({
            title: "{i18n>Filter}",
            content: new sap.ui.layout.form.SimpleForm({
              content: content,
            }),
            buttons: buttons
          });
          resolve(control);
        })
      },
      getSorter: (name, controller, tx) => {
        return new Promise(resolve => {
          let buttons = [
            new sap.m.Button({
              text: "{i18n>Save}",
              type: "Accept",
              press: (e) => {
                tx ? controller.onSaveCol(e) : controller.onCreateCol(e);
                controller.fnDestroy(name);
              }
            }),
            new sap.m.Button({
              text: "{i18n>Cancel}",
              type: "Default",
              press: (e) => controller.fnDestroy(name)
            })
          ]
          let content = [
            new sap.m.Label({
              text: "{i18n>Column}"
            }),
            new sap.m.ComboBox({
              items: {
                path: "table>/columns",
                template: new sap.ui.core.Item({
                  key: "{table>name}",
                  text: "{table>name}"
                })
              },
              value: "{item>/column}"
            }),
            new sap.m.Label({
              text: "{i18n>Operator}"
            }),
            new sap.m.ComboBox({
              items: {
                path: "const>/bOperators",
                template: new sap.ui.core.Item({
                  key: "{const>name}",
                  text: "{const>name}"
                })
              },
              value: "{item>/operator}"
            }),
            new sap.m.Label(),
          ]
          if (tx) content.push(new sap.m.Button({
            text: "{i18n>Delete}",
            type: "Reject",
            press: (e) => controller.fnDel(e)
          }));
          let control = new sap.m.Dialog({
            title: "{i18n>Sorter}",
            content: new sap.ui.layout.form.SimpleForm({
              content: content,
            }),
            buttons: buttons
          });
          resolve(control);
        })
      }
    }
    return module;
});
