sap.ui.define([
    "com/ventia/security_app/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "com/ventia/security_app/common/fragment.definition",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "com/ventia/security_app/common/dependencies/xlsx/jszip",
    "com/ventia/security_app/common/dependencies/xlsx/xlsx",
], function(Controller, JSONModel, FragmentDefinition, MessageToast, MessageBox, _JSZIP, _XLSX) {
    class Progress {
        constructor(controller, title, text) {
            this.controller = controller;
            this.title = title;
            this.text = text;
            this.indicator = new sap.m.ProgressIndicator({
                percentValue: 0,
                displayValue: "0%",
                showValue: true,
                state: "None",
                displayAnimation: true
            });
            this.instance = null;
            this.cancelled = false;
        }
        open() {
            this.instance = new sap.m.Dialog({
                type: sap.m.DialogType.Message,
                title: this.title,
                state: sap.ui.core.ValueState.Information,
                content: [
                    new sap.m.Text({
                        text: this.text
                    }),
                    this.indicator
                ],
                beginButton: new sap.m.Button({
                    type: sap.m.ButtonType.Emphasized,
                    text: this.controller.i18n("Cancel"),
                    press: () => {
                        this.cancelled = true;
                        this.instance.close();
                    }
                })
            });
            this.instance.open();
        }
        close() {
            this.instance.close();
        }
        setText(text) {
            this.text = text;
            let oText = this.instance.getContent()[0];
            oText.setText(this.text);
        }
        setTitle(title) {
            this.title = title;
            this.instance.setTitle(this.text);
        }
        setPercentValue(percentValue) {
            this.indicator.setPercentValue(percentValue);
            this.indicator.setDisplayValue(`${percentValue}%`);
        }
        setCancelled(cancelled) {
            this.cancelled = cancelled;
        }
        getCancelled() {
            return this.cancelled;
        }
    }
    return Progress;
});